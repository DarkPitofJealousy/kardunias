#!/usr/bin/env python
# -*- coding: utf-8 -*-
import source.misc as misc

def outro(player):
    #Die Outro Cutscene des Spiels, printet eigentlich nur Strings und beendet die Geschichte
    misc.countdown(20, fastrise=True)
    misc.write(u"Die..dd..die Erde bebt ganz fürchterlich!")
    misc.countdown(20, fastrise=True)
    misc.write(u"Der..der Turm!!! Er bricht in sich zusammen!")
    misc.countdown(20, fastrise=True)
    misc.write("DER TURM ZU BABYLON FÄLLT!")
    misc.countdown(5, rise=True)
    print("...")
    misc.countdown(5, rise=True)
    print(u"KARDUNIAS - DAS TEXT-ADVENTURE")
    misc.countdown(5, rise=True)
    print(u"Herzlichen Glückwunsch.")
    misc.countdown(5, rise=True)
    print(u"Du hast dich als würdig erwiesen.")
    misc.countdown(5, rise=True)
    print("...")
    misc.countdown(20, fastrise=True)
    itemcount = len(player.invlist)
    misc.write(u"Dann lass mal sehen... du hast...\n{x} von 8 optionalen Items gefunden!".format(x=itemcount))
    if itemcount == 8:
        misc.write(u"100 %! Du bist ein WAHRER König!")
    elif itemcount == 7:
        misc.write(u"Knapp! Hast du dich überall umgeschaut? Ein beachtenswertes Ergebnis!")
    elif itemcount <=6:
        misc.write(u"Das schreit doch nach einem erneuten Spieledurchlauf! Schaue dich in jedem Raum um oder male die Möglichkeiten mit, um alles zu entdecken!")
    if player.rh >= player.sc and player.rh >= player.wh:
        misc.write(u"Du wirst als Rhetoriker in die Annalen der Geschichte eingehen.")
    elif  player.sc >= player.wh and player.sc >= player.wh:
        misc.write(u"Du wirst als gefürchteter O.G. in die Annalen der Geschichte eingehen.")
    elif  player.wh >= player.sc and player.wh >= player.rh:
        misc.write(u"Du wirst als weiser Meister in die Annalen der Geschichte eingehen.")
    misc.countdown(20, fastrise=True)
    print("lalalalaaaa, lalala!")
    misc.countdown(3, rise=True)
    print("ladidaaaa, didadu!")
    misc.countdown(3, rise=True)
    misc.write(u"Die Sonne kitzelt dir auf der Nase. Du liegst in deinem gemütlichen Bett. \nDie Bardin singt und lacht.")
    misc.countdown(2, rise=True)
    print(u"Man hat dir dein morgentliches Frühstück gebracht!")
    misc.countdown(2, rise=True)
    misc.write(u"Du hast dich als würdig erwiesen, {x}.\nDu hast deine Königsprüfung bestanden.".format(x=player.name[:1].upper()+player.name[1:]))
    misc.write("Du kannst jetzt aus deinem Albtraum erwachen.\nBabylon möge unter deiner Herrschaft zu neuem Glanz und Reichtum gelangen!")
    misc.countdown(2)
    misc.countdown(5, rise=True)
    misc.countdown(30, fastrise=True)
    print(u"\t\t\tCREDITS")
    misc.countdown(2, rise=True)
    print("PROGRAMMING, STORY - DAVID WENZEL")
    misc.countdown(1, rise=True)
    print("ARCHITECTURE - PHILLIP TSE")
    misc.countdown(1, rise=True)
    print("PLAYTESTING - PATRICK RUDOLF")
    misc.countdown(3)
    print("THANK YOU FOR PLAYING KARDUNIAS - YOUR CAPSLABS TEAM")
    misc.countdown(3)
    misc.countdown(30, fastrise=True)
    quit()
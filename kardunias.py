#!/usr/bin/env python
# -*- coding: utf-8 -*-

#Als erstes wird das Intro importiert, welches ASCII Art und eine Namensabfrage enthält
import source.intro as intro
#dann die Engine, welche alle Objekte erstellt und die Spielereingaben "sortiert"
from source.engine import Engine
#Dieses Modul erlaubt es uns, Dinge zu loggen
from source.dev_tools import log, set_log_level
#Und dieses, Startparameter zu empfangen
import sys

if __name__ == '__main__':
    if "-v" in sys.argv:
        set_log_level(True)
        set_log_level("verbose")
        log("Running in verbose mode", "info")
    elif "--debug" in sys.argv:
        set_log_level(True)
        set_log_level("info")
        log("Running in debug mode (except for this single info)", "info")
        set_log_level("debug")
    username = intro.intro_sequence(fast=False)
    #Nachdem Intro ist der Nutzername bekannt, mit diesem kann die Engine gestartet werden.
    #Diese kann mit dem Namen (mithilfe einer Speicherstand-.txt oder neu erstellt) ein Charakter Objekt und alles weitere erstellen, printen und input entgegennehmen
    engine = Engine(username, [], "iwillbeauser", "iwillbearoom")
     #Die Engine nimmt vier Parameter entgegen, ihr Name, eine Liste (die legale Keywords darstellen wird), ein Userobjekt und ein Raumobjekt, welches noch Rauminhaltsobjekte enthalten wird
    engine.generate_player()
    engine.build(0)
    #Es wird einmalig bevor es losgeht ein Spieler kreiert und "build" aufgerufen (für die Eklärung, siehe "source/engine.py")
    #Das ganze Spiel läuft über eine Endlosschleife, die "handlde_input" aufruft, also endlos Nutzerinput entgegennimmt, bis das Spiel vorbei ist und in Outro beendet wird
    while True :
    	engine.handle_input()

Suggerieren     Suggeriert deinem Gegenüber, dass ein Kampf um Leben und Tod der falsche Weg ist!
Überzeugen      Du überzeugst dein Gegenüber, dass Friede und Glückselichkeit dem Kampf überlegen sind!
Rede halten     Du hältst eine Friedensrede, die nobelpreiswürdig wäre. 'Was ist ein Nobelpreis?' fragt sich der König. 
Intuition       Deine Intuition verrät dir die Schwächen deines Gegenübers. Liegt oft falsch. Erhöht kurzzeitig Rhetorik, Schikane und Weisheit. Die Erkenntnis geht verloren, wenn man vergiftet wird oder einschläft. Heilt Statusänderungen. 
Nachschlagen    Du schlägst die Schwächen deines Gegenübers nach! Oft ist die manuelle Enzyklopädiesuche sehr umständlich... Erhöht kurzzeitig stark Rhetorik, Schikane und Weisheit. Die Erkenntnis geht verloren, wenn man vergiftet wird oder einschläft. Heilt Statusänderungen. 
Wikipedia       Du durchsuchst Wikipedia nach den Schwächen deines Gegenübers. 'Was ist Wikipedia, und woher hab ich ein Smartphone?' Steigert Rhetorik, Schikane und Weisheit enorm. Die Erkenntnis geht verloren, wenn man vergiftet wird oder einschläft. Heilt Statusänderungen.
Trick-17        Der König schlägt die Gegner in die Flucht mit einem Standard-Diss, welches ein Halbton höher als das normale D is'. 
Tyrannisieren   Der König schlägt die Gegner mit dem Zorn seiner Tyrannei in die Flucht! Effektiver als Trick-17.       
Wutrede         Der König hält eine Wutrede, welche Diktatoren verblassen lassen würde! Schlägt Gegner am effektivsten in die Flucht. 
32-Heb-Auf      Der König fragt: "Kennt du 32-Heb-Auf?" und lässt sein Kartendeck im hohen Bogen durch den Raum fliegen. Der Gegner darf das spannende Spiel spielen. Heilt und gewinnt Zeit, um mehr Beleidigungen aus dem Gedächtnis zu kramen. Allerdings vergisst man sie, wenn man vergiftet wird oder einschläft. Heilt auch Statusänderungen.
Posieren        Der König posiert mit lässiger Schulterhaltung und Sonnenbrille. Heilt mehr HP und bringt mehr Beleidigungen hervor. Allerdings vergisst man sie, wenn man vergiftet wird oder einschläft. Heilt Statusänderungen. 
Sibirischer Doppelbluff Erst tut er so als würde er es machen, macht es dann aber doch nicht. Der Gegner hat das kommen sehen. Dann macht er es doch wie ursprünglich! Sieht niemand kommen! Heilt am meisten HP und sorgt für die besten Beleidigungen! Allerdings vergisst man sie, wenn man vergiftet wird oder einschläft. Heilt auch Statusänderungen. 
Demoralis       Ein Zauberbann, welcher den Geist des Gegenübers zerfallen lässt. Erfordert hohe Konzentration.
Anima Vincere   Ein mächtiger Zauberbann, welche den Geist des Gegenübers komplett einnimmt. Erfordert höchste Konzentration.
Leckerli        Der mächtigste Weg, den Geist des Gegenübers zu manipulieren! Ein Leckerli! Da kann keiner widerstehen.
Abrakapflaster  Ein... Wundheilzauber... (aus dem Medizinkasten). Heilt ein drittel aller Wunden. 
3xSchwarzer-1.-Hilfe-Kasten Ein mächtiger 'Wundheilzauber' ... (ein .. Medizinkasten..). Heilt etwa die Hälfte aller Wunden.
Simsala-Wundheilzauber  Ein ECHTER Wundheilzauber! Na sowas! Heilt MEHR als die Hälfte aller Wunden!
